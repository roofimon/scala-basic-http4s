name := "my-http4s-project"
version := "0.1"
scalaVersion := "2.13.12"
val http4sVersion = "0.23.25"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-ember-server" % http4sVersion, // Replace with the actual version
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-ember-client" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion
)
